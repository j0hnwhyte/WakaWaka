#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include "WakeOnLan.h"
#define BUFSIZE 255

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0x2E, 0xEC};
IPAddress ip(10, 80, 12, 251);

// Initialize the Ethernet server library
EthernetServer server(80);
EthernetUDP udp;

void setup() {
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());

  udp.begin(7);
}


void loop() {
  char clientline[BUFSIZE];
  int index = 0;
  
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    while (client.connected()) {
      if (client.available()) {
        // Fill the buffer
        char c = client.read();
        if(c != '\n' && c != '\r' && index < BUFSIZE){
          clientline[index++] = c;
          continue;
        }
        clientline[index++] = 0;

        Serial.println(clientline);

        // convert clientline into a proper string for further processing
        String urlString = String(clientline);

        //  extract the operation
        String op = urlString.substring(0,urlString.indexOf(' '));

        //  we're only interested in the first part...
        urlString = urlString.substring(urlString.indexOf('/'), urlString.indexOf(' ', urlString.indexOf('/')));
        urlString.toUpperCase();

        String macString = urlString.substring(urlString.lastIndexOf('/') + 1, urlString.length());

        if (macString.length() != 17) {
          sendRequestBad(&client);
        } else {
          // Request OK, proceed decoding
          byte mac[6];
          for (int i = 0; i < 6; i++) {
            String by = macString.substring(3 * i, (3 * i) + 2);
            mac[i] = strtoul(by.c_str(), NULL, 16);
          }
          char cmd[80];
          sprintf(cmd, "waking %0x:%0x:%0x:%0x:%0x:%0x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
          Serial.println(cmd);
          WakeOnLan::send(mac, 6, udp);

          sendRequestOk(&client);
        }

        break;
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }
}

int sendRequestBad(EthernetClient *client) {
  client->println("HTTP/1.1 400 Bad Request");
  client->println("Content-Type: text/html");
  client->println("Connection: close");
  client->println();
  client->println("<!DOCTYPE HTML>");
  client->println("<html>");
  client->println("Bad request");
  client->println("</html>");
}

int sendRequestOk(EthernetClient *client) {
  client->println("HTTP/1.1 200 OK");
  client->println("Content-Type: text/html");
  client->println("Connection: close");
  client->println();
  client->println("<!DOCTYPE HTML>");
  client->println("<html>");
  client->println("Command received");
  client->println("</html>");
}

